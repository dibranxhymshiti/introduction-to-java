public class ArithmeticOperations {
    public static void main(String[] args) {
        int x = 6;
        int y = 4;

        System.out.println("Starting value of X: " + x);
        System.out.println("Starting value of Y: " + y);

        int addition = x + y;
        int subtraction = x - y;
        int multiplication = x * y;
        double division = x / y;
        int modulo = x % y;

        System.out.println("Addition: " + addition);
        System.out.println("Subtraction: " + subtraction);
        System.out.println("Multiplication: " + multiplication);
        System.out.println("Division: " + division);
        System.out.println("Modulo: " + modulo);

        x++;
        System.out.println("Value of X after increment: " + x);
        y--;
        System.out.println("Value of Y after decrement: " + y);

        x *= 2;
        System.out.println("Value of X after multiplication: " + x);
        y /= 3;
        System.out.println("Value of Y after division: " + y);

        // Todo: Do a modulo of x
        // x = x % 2;
        x %= 2;
        System.out.println("Value of X after modulus: " + x);

    }
}
